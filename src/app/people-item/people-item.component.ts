import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-people-item',
  templateUrl: './people-item.component.html',
  styleUrls: ['./people-item.component.css']
})


export class PeopleItemComponent implements OnInit {
  @Input()
  inputResult:Object;

 
  constructor() { }

  ngOnInit() {
  }

}


export class Person {
  public name: string;
  public height: number;
  public mass: number;
  public hair_color: string;

  constructor (name : string, height: number, mass : number, hair_color: string) {
    this.name = name;
    this.height = height;
    this.mass = mass;
    this.hair_color = hair_color;
  }
}


