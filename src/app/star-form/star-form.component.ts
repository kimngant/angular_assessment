import { Component, OnInit } from '@angular/core';
import { StarWar, StarService } from '../common/star-service/star-service';
import { Person } from '../people-item/people-item.component';

@Component({
  selector: 'app-star-form',
  templateUrl: './star-form.component.html',
  styleUrls: ['./star-form.component.css']
})
export class StarFormComponent implements OnInit {

  public option : string;
  public id : number;
  public starWar : Object;
  

  options = ['People', 'Planet',
            'Vehicles', 'Scpecies', 'Starships'];

  id_options = [1,2,3,4,5];

  submitted = false;

  onSubmit() { 
    this.submitted = true; 
    console.log("clicked on choice");
    console.log(this.option );
    console.log( this.id );

    this.ss.getPeople(this.id).subscribe(response => {
      this.starWar = response;
      console.log("results received");
      console.log(this.starWar)
      
    }); 
  }
  
  constructor(private ss: StarService) { 
  
  }

  ngOnInit() {
  }

}
