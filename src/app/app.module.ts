import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule }   from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StarFormComponent } from './star-form/star-form.component';
import { HomeComponent } from './home/home.component';

import { StarService } from './common/star-service/star-service';
import { StarWarItemComponent } from './star-war-item/star-war-item.component';
import { PeopleItemComponent } from './people-item/people-item.component';

@NgModule({
  declarations: [
    AppComponent,
    StarFormComponent,
    HomeComponent,
    StarWarItemComponent,
    PeopleItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [StarService],
  bootstrap: [AppComponent]
})
export class AppModule { }
