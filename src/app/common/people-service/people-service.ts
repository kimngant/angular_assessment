import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export class People {
    constructor(
        public name: string,
        public height: number,
        public mass: number,
        public hair_color: string) {           
        }
}

@Injectable()
export class PeopleService {
    
    people : People[] = [];

    constructor(private httpClient:HttpClient ) { }
 
    getPeople(): Observable<People[]> {
        return this.httpClient.get<People[]>('https://swapi.co/api/people');
    }

}
