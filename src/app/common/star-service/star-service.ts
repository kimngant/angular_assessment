import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export class StarWar {
    constructor(
        public id: number,
        public title: string
        
        ) {
    }
}

@Injectable()
export class StarService {
    
    starWar : Object;

    constructor(private httpClient:HttpClient ) { }
 
    getPeople( id: number): Observable<Object> {
        return this.httpClient.get<Object>('https://swapi.co/api/people/' + id);
    }

    getPlanet(): Observable<StarWar[]> {
        return this.httpClient.get<StarWar[]>('https://swapi.co/api/planet');
    }
    
    getFilm(): Observable<StarWar[]> {
        return this.httpClient.get<StarWar[]>('https://swapi.co/api/film');
    }
       
}
