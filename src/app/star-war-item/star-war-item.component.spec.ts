import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StarWarItemComponent } from './star-war-item.component';

describe('StarWarItemComponent', () => {
  let component: StarWarItemComponent;
  let fixture: ComponentFixture<StarWarItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StarWarItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StarWarItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
